package com.tayamino.partcheese.backend;

import android.app.Application;
import android.content.Context;

import com.parse.Parse;
import com.parse.ParseObject;

import com.tayamino.partcheese.object.MenuType;
import com.tayamino.partcheese.object.MenuItem;

public class CustomApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Context ctx = getApplicationContext();

        Parse.enableLocalDatastore(ctx);

        ParseObject.registerSubclass(MenuType.class);
        ParseObject.registerSubclass(MenuItem.class);

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("partcheese")
                // if defined
                .clientKey("9d114265e6044e94")
                .server("https://partcheese.herokuapp.com/model/api")
                .build()
        );
    }
}
