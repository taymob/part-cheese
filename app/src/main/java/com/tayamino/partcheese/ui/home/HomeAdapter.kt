package com.tayamino.partcheese.ui.home;

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tayamino.partcheese.EntryDetailActivity
import com.tayamino.partcheese.EntryDetailFragment
import com.tayamino.partcheese.R
import com.tayamino.partcheese.`object`.MenuItem

class HomeAdapter(
        private val parentActivity: HomeFragment,
        private val values: List<MenuItem>,
        private val twoPane: Boolean
) :
        RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener

        init {
                onClickListener = View.OnClickListener { v ->
                        val item = v.tag as MenuItem
                        if (twoPane) {
                                val fragment = EntryDetailFragment().apply {
                                        arguments = Bundle().apply {
                                                putString(EntryDetailFragment.ARG_ITEM_ID, item.objectId)
                                        }
                                }
                                parentActivity.parentFragmentManager
                                        .beginTransaction()
                                        .replace(R.id.entry_detail_container, fragment)
                                        .commit()
                        } else {
                                val intent = Intent(v.context, EntryDetailActivity::class.java).apply {
                                        putExtra(EntryDetailFragment.ARG_ITEM_ID, item.objectId)
                                }
                                v.context.startActivity(intent)
                        }
                }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.entry_list_content, parent, false)
                return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                val item = values[position]
                holder.idView.text = item.objectId
                holder.contentView.text = item.displayName

                with(holder.itemView) {
                        tag = item
                        setOnClickListener(onClickListener)
                }
        }

        override fun getItemCount() = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
                val idView: TextView = view.findViewById(R.id.id_text)
                val contentView: TextView = view.findViewById(R.id.content)
        }
}
