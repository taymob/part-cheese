package com.tayamino.partcheese.object;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Menu")
public class MenuItem extends ParseObject {
    public String getDisplayName() {
        return getString("displayName");
    }
    public void setDisplayName(String value) {
        put("displayName", value);
    }
    public String toString() {
        return getDisplayName();
    }
}
