package com.tayamino.partcheese.object;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Type")
public class MenuType extends ParseObject {
    public String getDisplayName() {
        return getString("displayName");
    }
    public void setDisplayName(String value) {
        put("displayName", value);
    }
}
