package com.tayamino.partcheese.ui.gallery

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.tayamino.partcheese.EntryDetailActivity
import com.tayamino.partcheese.EntryDetailFragment
import com.tayamino.partcheese.EntryListActivity
import com.tayamino.partcheese.R
import com.tayamino.partcheese.`object`.MenuItem
import com.tayamino.partcheese.`object`.MenuType
import com.tayamino.partcheese.dummy.Catalogue
import com.tayamino.partcheese.dummy.Features

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
                ViewModelProvider(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        setupRecyclerView(root.findViewById(R.id.gallery_list))
        return root
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter =
            GalleryAdapter(this, Catalogue.ITEMS, false)
    }
}