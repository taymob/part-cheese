package com.tayamino.partcheese.dummy

import android.util.Log
import java.util.*
import com.parse.ParseQuery
import com.tayamino.partcheese.`object`.MenuType

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object Catalogue {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<MenuType> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, MenuType> = HashMap()

    init {
        val query = ParseQuery.getQuery<MenuType>("MenuType")
        //query.whereEqualTo("playerName", "Dan Stemkoski")
        //query.fromLocalDatastore()
        query.findInBackground { menuList, e ->
            if (e == null) {
                for (menu in menuList) {
                    addItem(menu)

                    Log.d("score", "Retrieved $menu ...")

                    menu.unpinInBackground()
                }
            } else {
                Log.d("score", "Error: " + e.message)
            }
        }
    }

    private fun addItem(item: MenuType) {
        ITEMS.add(item)
        ITEM_MAP.put(item.objectId, item)
    }
}